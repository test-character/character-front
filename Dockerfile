FROM node:12.18-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN npm install --unsafe-perm=true --allow-root

EXPOSE 8080

CMD ["npm", "start"]