module.exports = {
    collectCoverage: true,
    collectCoverageFrom: [
        "<rootDir>/src/**/*.{js,jsx}",
        "!<rootDir>/dist/",
        "!<rootDir>/node_modules/",
        "!<rootDir>/public/",
        "!<rootDir>/coverage/"
    ],
    coverageThreshold: {
        global: {
            branches: 80,
            functions: 80,
            lines: 80,
            statements: 80
        }
    }
};
