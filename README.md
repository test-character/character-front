# Web Character
Web allows to display the list of Rick and Morty characters

# Getting Started And Installing
After cloning the project, enter the project folder and execute:
```
docker build -t <IMAGENAME> .
```

```
docker run -p 8080:8080 -h 0.0.0.0 -t <IMAGENAME>
```

# Run Local
```
npm run start
```

```
Running on http://localhost:8080/
```

# Run Build
```
npm run build
```

# Coverage
```
npm run test
```

# Linter
```
npm run linter:check
```