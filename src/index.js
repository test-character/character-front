import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { configureStore } from "./Store/ConfigureStore";
import Routes from "./Pages/Routes";
import ErrorBoundary from "./Containers/ErrorBoundary";

const store = configureStore();

ReactDOM.render(
    <ErrorBoundary>
        <Provider store={store}>
            <Routes />
        </Provider>
    </ErrorBoundary>,
    document.getElementById("app")
);
