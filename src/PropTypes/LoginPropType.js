import PropTypes from 'prop-types';

const LoginPropTypes = {
  username: PropTypes.string,
  password: PropTypes.string,
  classes: PropTypes.instanceOf(Object)
};

export default LoginPropTypes;
