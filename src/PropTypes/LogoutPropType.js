import PropTypes from 'prop-types';

const LogoutPropTypes = {
  error: PropTypes.string,
  logout: PropTypes.func,
};

export default LogoutPropTypes;
