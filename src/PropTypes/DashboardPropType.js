import PropTypes from 'prop-types';

const DashboardPropType = {
  classes: PropTypes.instanceOf(Object).isRequired
};

export default DashboardPropType;
