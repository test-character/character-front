import PropTypes from 'prop-types';

const CardPropType = {
  item: PropTypes.instanceOf(Object),
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default CardPropType;
