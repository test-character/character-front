import PropTypes from 'prop-types';

const MassagePropType = {
  message: PropTypes.string.isRequired,
  flag: PropTypes.string.isRequired,
  classes: PropTypes.instanceOf(Object).isRequired,
};

export default MassagePropType;
