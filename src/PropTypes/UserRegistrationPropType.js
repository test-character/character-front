import PropTypes from 'prop-types';

const UserRegistrationPropType = {
  name: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  password: PropTypes.string,
  confirmPassword: PropTypes.string,
  classes: PropTypes.instanceOf(Object).isRequired
};

export default UserRegistrationPropType;
