import Character from "../Api/Character";

export const ACTIONS = {
    FETCH_CHARACTERS: "character/fetch_characters",
    FETCH_CHARACTERS_ERROR: "character/fetch_characters_error"
};

export const getCharacters = () => async (dispatch) => {
    return await Character.getCharacters()
        .then((response) => {
            dispatch({
                type: ACTIONS.FETCH_CHARACTERS,
                payload: response.data
            });
            return response.data;
        })
        .catch((error) => {
            const {
                response: {
                    data: {
                        status: { message }
                    }
                }
            } = error;
            dispatch({ type: ACTIONS.FETCH_CHARACTERS_ERROR, payload: message });
        });
};
