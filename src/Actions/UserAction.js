import User from "../Api/User";

export const ACTIONS = {
    USER_REGISTER_SUCCESS: "user/user_register_success",
    USER_REGISTER_ERROR: "user/user_register_error",
    USER_REGISTER_CLEAR_FORM: "user/user_register_clear_form"
};

export const registerUser = (payload) => async (dispatch) => {
    return await User.register(payload)
        .then((response) => {
            dispatch({
                type: ACTIONS.USER_REGISTER_SUCCESS,
                payload: response.data
            });
            return response.data
        })
        .catch((error) => {
            const {
                response: {
                    data: {
                        status: { message }
                    }
                }
            } = error;
            dispatch({ type: ACTIONS.USER_REGISTER_ERROR, payload: message });
        });
};

export function clearForm() {
    return (dispatch) =>
        dispatch({
            type: ACTIONS.USER_REGISTER_CLEAR_FORM
        });
}
