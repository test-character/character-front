import Auth from "../Api/Auth";

export const ACTIONS = {
    LOGIN_SUCCESS: "auth/login_success",
    LOGIN_ERROR: "auth/login_error",
    LOGIN_CLEAR_FORM: "auth/login_clear_form",
    LOGOUT: "auth/logout"
};

export const login = (payload) => async (dispatch) => {
    await Auth.login(payload)
        .then((response) => {
            dispatch({ type: ACTIONS.LOGIN_SUCCESS, payload: response.data });
        })
        .catch((error) => {
            const {
                response: {
                    data: {
                        status: { message }
                    }
                }
            } = error;
            dispatch({ type: ACTIONS.LOGIN_ERROR, payload: message });
        });
};

export function logout() {
    return (dispatch) =>
        dispatch({
            type: ACTIONS.LOGOUT
        });
}

export function clearForm() {
    return (dispatch) =>
        dispatch({
            type: ACTIONS.LOGIN_CLEAR_FORM
        });
}
