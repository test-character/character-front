import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';

import MainLayout from '../Containers/MainLayout';
import HomeLayout from '../Containers/HomeLayout';
import Dashboard from '../Containers/Dashboard';
import Login from '../Containers/Login';
import UserRegistration from '../Containers/User/UserRegistration';
import NotFound from '../Components/NotFound';
import Logout from '../Components/Logout';

const validAuth = () => {
  const token = localStorage.getItem('token');
  let authenticated = false;
  if (token) {
    authenticated = true;
  }
  return authenticated;
};

const PublicRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={props =>
        !validAuth() ? (
          <MainLayout>
            <Component {...props} />
          </MainLayout>
        ) : (
          // eslint-disable-next-line react/prop-types
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        )
      }
    />
  );
};

const PrivateRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={props =>
        validAuth() ? (
            <HomeLayout>
              <Component {...props} />
            </HomeLayout>
        ) : (
          <Redirect to="/login" />
        )
      }
    />
  );
};


class Routes extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <PrivateRoute exact path="/" component={Dashboard} />
          <PublicRoute exact path="/login" component={Login} />
          <PublicRoute exact path="/user/register" component={UserRegistration} />
          <PrivateRoute exact path="/logout" component={Logout} />
          <PublicRoute exact component={NotFound} />
        </Switch>
      </Router>
    );
  }
}


export default Routes;
