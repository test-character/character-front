import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { logout } from '../../Actions/AuthAction';
import LogoutPropTypes from '../../PropTypes/LogoutPropType';

export class Logout extends React.Component {
  constructor(props) {
    super(props);
    this.props.logout();
    this.props.history.push('/login');
  }

  render = () => null;
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  logout,
};

Logout.propTypes = LogoutPropTypes;

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Logout),
);
