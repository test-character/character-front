import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import MessagePropType from "../../PropTypes/MessagePropType";
import { customStyles } from "./style";

const Message = ({ message, flag, classes, onClose }) => (
    <div className={classes.root}>
        {flag === 0 ? (
            <Alert
                severity="info"
                action={
                    <Button color="inherit" size="small" onClick={onClose}>
                        x
                    </Button>
                }
            >
                {message}
            </Alert>
        ) : (
            <Alert
                severity="error"
                action={
                    <Button color="inherit" size="small" onClick={onClose}>
                        x
                    </Button>
                }
            >
                {message}
            </Alert>
        )}
    </div>
);

Message.propTypes = MessagePropType;

export default withStyles(customStyles)(Message);
