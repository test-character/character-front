import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { customStyles } from './style';

export class Header extends React.Component {

  logout = () => {
    this.props.history.push("/logout");
  }

  render() {
    // eslint-disable-next-line react/prop-types
    const { classes } = this.props;

    return (
      <header>     
      <AppBar position="static" className={classes.appBarHeader}>
        <Toolbar>
        <div className={classes.menuButton}></div>
          <Typography variant="h6" className={classes.title}>
            Characters
          </Typography>
          <Button color="inherit" onClick={() => {
            this.logout();
        }}>Logout</Button>
        </Toolbar>
      </AppBar>
      </header>
    );
  }
}

export default withRouter(withStyles(customStyles)(Header));