export const customStyles = (theme) => ({
    root2: {
        width: "100%",
        "& > * + *": {
            marginTop: theme.spacing(5)
        }
    },
    card: {
        maxWidth: '100%'
    },
    media: {
        height: 300
    }
});
