import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardPropType from "../../PropTypes/CardPropType";
import { customStyles } from "./style";

const InfoCard = ({ item, classes }) => (
    <div className={classes.root2}>
        <Card className={classes.card}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={item.image}
                    title="Character"
                />
                <CardContent>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="h1"
                    >
                        <b>Name:</b> {item.name}
                    </Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="h1"
                    >
                        <b>Status:</b> {item.status}
                    </Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="h1"
                    >
                        <b>Species:</b> {item.species}
                    </Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="h1"
                    >
                        <b>Gender:</b> {item.gender}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    </div>
);

InfoCard.propTypes = CardPropType;

export default withStyles(customStyles)(InfoCard);
