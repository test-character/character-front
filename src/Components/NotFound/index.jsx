import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import { customStyles } from './style';

const redirect = () => {
  window.location = '/';
  return 0;
};

// eslint-disable-next-line react/prop-types
const NotFound = ({ classes }) => (
    // eslint-disable-next-line react/prop-types
    <div className={classes.root}>
      <Alert severity="warning"
        action={
        <Button color="inherit" size="small" onClick={redirect}>
          To return
        </Button>
      }>
        Page not found!
      </Alert>
    </div>
);

export default withStyles(customStyles)(NotFound);

