import React, { Component } from 'react';

class MainLayout extends Component {
  render() {
    // eslint-disable-next-line react/prop-types
    const { children } = this.props;
    return (
      <div className="main-layout">
        <div>{children}</div>
      </div>
    );
  }
}

export default MainLayout;
