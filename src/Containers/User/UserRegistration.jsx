import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import MessageStyles from "../../Components/Message";
import { registerUser, clearForm } from "../../Actions/UserAction";
import UserRegistrationPropType from "../../PropTypes/UserRegistrationPropType";
import { customStyles } from "./style";

export class UserRegistration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            lastName: "",
            username: "",
            password: "",
            confirmPassword: ""
        };
    }

    handleChange = (e) => {
        let attr = e.target.name;
        let value = e.target.value;
        this.setState({ [attr]: value });
    };

    hiddenMessage = () => {
        this.props.clearForm();
    };

    handleSubmit = async (e) => {
        e.preventDefault();
        const dataUser = await this.props.registerUser(this.state);
        if (dataUser) {
            this.props.history.push("/login");
        }
    };
    handleBack = async (e) => {
        e.preventDefault();
        this.props.history.push("/login");
    };

    render() {
        const { classes, error, message } = this.props;
        return (
            <div className={classes.userRegistration}>
                <div className={classes.divForm}>
                    {error && (
                        <MessageStyles
                            message={message}
                            flag="1"
                            classes={classes}
                            onClose={this.hiddenMessage}
                        />
                    )}
                    <form
                        className={classes.root}
                        noValidate
                        autoComplete="off"
                    >
                        <Grid container spacing={4}>
                            <Grid item xs={6}>
                                <TextField
                                    id="name"
                                    name="name"
                                    label="Name"
                                    placeholder="Name"
                                    multiline
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="lastName"
                                    name="lastName"
                                    label="Last Name"
                                    placeholder="Last Name"
                                    multiline
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="username"
                                    name="username"
                                    label="Username"
                                    placeholder="Username"
                                    multiline
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="password"
                                    name="password"
                                    label="Password"
                                    placeholder="Password"
                                    type="password"
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    label="Confirm Password"
                                    placeholder="Confirm Password"
                                    type="password"
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.alignButton}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleBack}
                                >
                                    Back
                                </Button>
                            </Grid>
                            <Grid item xs={6} className={classes.alignButton}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleSubmit}
                                >
                                    Register
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.userReducer.user,
    message: state.userReducer.message,
    error: state.userReducer.error
});

const mapDispatchToProps = {
    registerUser,
    clearForm
};

UserRegistration.propTypes = UserRegistrationPropType;

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withStyles(customStyles)(UserRegistration))
);
