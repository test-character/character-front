import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import MessageStyles from "../../Components/Message";
import { login, clearForm } from "../../Actions/AuthAction";
import LoginPropTypes from "../../PropTypes/LoginPropType";
import { customStyles } from "./style";

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        let attr = e.target.name;
        let value = e.target.value;
        this.setState({ [attr]: value });
    };

    hiddenMessage = () => {
        this.props.clearForm();
    };

    handleSubmit = async (e) => {
        e.preventDefault();
        await this.props.login(this.state);
        const { error } = this.props;
        if (!error) {
            this.props.history.push("/");
        }
    };

    render() {
        const { classes, message, error } = this.props;
        return (
            <div className={classes.login}>
                <div className={classes.divForm}>
                    {error && (
                        <MessageStyles
                            message={message}
                            flag="1"
                            classes={classes}
                            onClose={this.hiddenMessage}
                        />
                    )}
                    <form
                        className={classes.root}
                        noValidate
                        autoComplete="off"
                    >
                        <Grid container spacing={4} alignItems="center">
                            <Grid item xs={12}>
                                <TextField
                                    id="username"
                                    name="username"
                                    label="Username"
                                    placeholder="Username"
                                    multiline
                                    onChange={this.handleChange}
                                    className={classes.textFieldWidth}
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    id="password"
                                    name="password"
                                    label="Password"
                                    placeholder="Password"
                                    type="password"
                                    onChange={this.handleChange}
                                    className={classes.textFieldWidth}
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleSubmit}
                                    size="large"
                                >
                                    Login
                                </Button>
                            </Grid>

                            <Grid item xs={12}>
                                <Link to="/user/register">Register user</Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    username: state.authReducer.username,
    password: state.authReducer.password,
    data: state.authReducer.data,
    message: state.authReducer.message,
    error: state.authReducer.error
});

const mapDispatchToProps = {
    login,
    clearForm
};

Login.propTypes = LoginPropTypes;

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withStyles(customStyles)(Login))
);
