export const customStyles = theme => ({
    root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
          width: '25ch',
        },
      },
      textFieldWidth: {
        width: 300,
        maxWidth: 300,
      },
      login: {
        width: "50%",
        height: "100%",
        margin: '0 auto'
      },
      divForm: {
        padding: 50,
        textAlign: 'center',
      }
  });