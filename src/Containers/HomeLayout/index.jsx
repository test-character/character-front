import React, { Component } from 'react';
import Header from '../../Components/Header';
import Footer from '../../Components/Footer';

class HomeLayout extends Component {
  render() {
    // eslint-disable-next-line react/prop-types
    const { children } = this.props;
    return (
        <div>
        <Header />
        <div className="container">{children}</div>
        <Footer />
      </div>
    );
  }
}

export default HomeLayout;