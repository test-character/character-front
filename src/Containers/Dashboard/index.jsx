import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import MessageStyles from "../../Components/Message";
import InfoCard from "../../Components/InfoCard";
import { getCharacters } from "../../Actions/CharacterAction";
import DashboardPropType from "../../PropTypes/DashboardPropType";
import { customStyles } from "./style";

export class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            characters: []
        };
    }

    componentDidMount() {
        this.getCharacter();
    }

    async getCharacter() {
        await this.props.getCharacters();
        const { error } = this.props;
        if (error) {
            this.props.history.push("/logout");
        }
    }

    render() {
        const { classes, message, characters } = this.props;
        return (
            <div>
                <div className={classes.root}>
                    {characters.length > 0 ? (
                        <div className={classes.root}>
                            <Grid container spacing={3}>
                                {characters.map((item, index) => (
                                    <Grid item key={index} xs={4}>
                                        <InfoCard
                                            item={item}
                                            classes={classes}
                                        />
                                    </Grid>
                                ))}
                            </Grid>
                        </div>
                    ) : (
                        <MessageStyles
                            message={message}
                            flag="0"
                            classes={classes}
                            onClose={() => {}}
                        />
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    characters: state.characterReducer.characters,
    message: state.characterReducer.message,
    error: state.characterReducer.error
});

const mapDispatchToProps = {
    getCharacters
};

Dashboard.propTypes = DashboardPropType;

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withStyles(customStyles)(Dashboard))
);
