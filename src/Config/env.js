const env = () => {
    let ENV_URL = "";
    if (process) {
        const { NODE_ENV, API_URL } = process.env;
        if (API_URL) {
            return API_URL;
        }
        switch (NODE_ENV) {
            case "develop":
            case "master":
            default:
                ENV_URL = "http://localhost:3001";
                break;
        }
    }

    return ENV_URL;
};

const URL = env();
export default URL;
