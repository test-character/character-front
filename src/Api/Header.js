export default function headers() {
  const items = {};
  const token = localStorage.getItem('token');

  if (token) {
    items.Authorization = `Bearer ${token}`;
  }

  return items;
}
