import AXIOS from "../Config/Axios";
import headers from "./Header";

const Character = {
    getCharacters() {
        return AXIOS.get("/character", { headers: headers() });
    }
};

export default Character;
