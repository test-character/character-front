import AXIOS from "../Config/Axios";
import headers from "./Header";

const Auth = {
    login(payload) {
        return AXIOS.post(
            "/auth/login",
            { ...payload },
            { headers: headers() }
        );
    },
    logout() {
        return AXIOS.post("/auth/logout", { headers: headers() });
    }
};

export default Auth;
