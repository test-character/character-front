import AXIOS from "../Config/Axios";
import headers from "./Header";

const User = {
    register(payload) {
        return AXIOS.post(
            "/user/register",
            { ...payload },
            { headers: headers() }
        );
    }
};

export default User;
