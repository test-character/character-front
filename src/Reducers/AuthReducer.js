import { ACTIONS } from "../Actions/AuthAction";

const initialState = {
    username: "",
    password: "",
    data: [],
    authenticated: false,
    message: "",
    error: false
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTIONS.LOGIN_SUCCESS:
            localStorage.setItem("token", action.payload.data.token);
            return {
                ...state,
                data: action.payload.data.user,
                authenticated: true
            };
        case ACTIONS.LOGIN_ERROR:
            return {
                ...state,
                authenticated: false,
                message: action.payload,
                error: true
            };
        case ACTIONS.LOGIN_CLEAR_FORM:
            return {
                ...state,
                ...initialState
            };
        case ACTIONS.LOGOUT:
            localStorage.removeItem("token");
            return {
                ...state,
                ...initialState
            };
        default:
            return state;
    }
};

export default authReducer;
