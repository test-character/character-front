import { combineReducers } from 'redux';
import authReducer from './AuthReducer';
import characterReducer from './CharacterReducer';
import userReducer from './UserReducer';

const rootReducer = combineReducers({
  authReducer,
  characterReducer,
  userReducer
});

export default rootReducer;
