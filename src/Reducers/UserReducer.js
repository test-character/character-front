import { ACTIONS } from '../Actions/UserAction';

const initialState = {
  user: {},
  message: "",
  error: false
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.USER_REGISTER:
      return {
        ...state,
        user: action.payload.data.user
      };
    case ACTIONS.USER_REGISTER_ERROR:
      return {
        ...state,
        message: action.payload,
        error: true
      };
    case ACTIONS.USER_REGISTER_CLEAR_FORM:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
};

export default userReducer;
