import { ACTIONS } from '../Actions/CharacterAction';

const initialState = {
  characters: [],
  message: "",
  error: false
};

const characterReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.FETCH_CHARACTERS:
      return {
        ...state,
        characters: action.payload.data
      };
    case ACTIONS.FETCH_CHARACTERS_ERROR:
      return {
        ...state,
        message: action.payload,
        error: true
      };
    default:
      return state;
  }
};

export default characterReducer;
