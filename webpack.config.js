const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const ManifestPlugin = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const excludeRules = [/(node_modules)/, /dist/, /coverage/];

module.exports = {
  entry: "./src/index.js",
  output: {
    // eslint-disable-next-line no-undef
    path: path.resolve(__dirname, "./dist"),
    filename: "bundle.js",
    library: "bundle",
    libraryTarget: "umd",
  },
  resolve: { 
    extensions: [".js", ".jsx", ".css", "json"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: excludeRules,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
          },
        ],
      },
    ],
  },
  devServer: {
    contentBase: './dist',
    disableHostCheck: true,
    stats: { colors: true },
    historyApiFallback: true,
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./public/index.html",
      filename: "./index.html",
      favicon: "./public/favicon.ico",
    }),
    new ManifestPlugin({
        fileName: 'manifest.json',
    }),
    new CleanWebpackPlugin()
  ],
};